package godau.fynn.dsbdirect.test

import godau.fynn.dsbdirect.model.entry.Entry
import godau.fynn.dsbdirect.model.entry.EntryField
import godau.fynn.dsbdirect.table.reader.BlueWilli
import godau.fynn.dsbdirect.table.reader.ReaderFactory
import godau.fynn.dsbdirect.table.reader.Untis
import org.junit.Assert
import org.junit.Test
import java.io.PrintStream
import java.io.PrintWriter
import java.io.StringWriter

class ParserTest {

    @Test
    fun testBlueWilliCss() {
        val html = TestUtil.readDocument("/blue_willi_css.htm")

        val reader = ReaderFactory.getReader(null, "", html)

        check(reader is BlueWilli) { "Could not autodetect blue willi.css parser!" }

        reader.setHtml(html)

        TableRenderer.renderToLog(
            reader.read()
        )

        // ToDo: Check entries
    }
    @Test
    fun `issue 95`() {
        val html = TestUtil.readDocument("/issue_95.htm")

        val reader = ReaderFactory.getReader(null, "", html)

        check(reader is Untis) { "Wrong parser?!" }

        reader.setHtml(html)

        TableRenderer.renderToLog(reader.read())

        Assert.assertEquals(reader.read()[0][EntryField.INFO], "Die Klasse 5d hat diese Woche Pausendienst!")
    }
}
